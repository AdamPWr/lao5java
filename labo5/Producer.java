package labo5;

import javax.swing.JOptionPane;

public class Producer extends Thread
{
	  private char product = 'A';
	  private Buffer bufor;
	  private int id;
	  private boolean paused;
	  
	  public void play()
	  {
	    this.paused = false;
	  }
	  
	  public void pause()
	  {
	    this.paused = true;
	  }
	  
	  public Producer(Buffer c, int i)
	  {
	    this.bufor = c;
	    this.id = i;
	  }
	  
	  
	  
	  public void run()
	  {
		  
		while(paused == false)
		{
	    if (this.product > 'Z')
	    {
	      this.product = 'A';
	    }
	    this.bufor.put(this.id, this.product++);
	    try
	    {
	      sleep((int)(Math.random() * 1000.0D) + 200);
	    }
	    catch (InterruptedException e) {}
		}
	    while (this.paused) {
	      try
	      {
	        sleep(5L);
	      }
	      catch (InterruptedException e) {}
	    }
	    
	  }
}
