package labo5;

import java.awt.Component;
import java.awt.Font;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.html.HTMLDocument.Iterator;

public class ProducentKonsument extends JFrame implements ActionListener 
{

	public static void main(String[] args)
	  {
	    new ProducentKonsument();
	  }
	private static final long serialVersionUID = 1L;
	
	  private boolean started;
	  
	  private List<Producer> producenci = new ArrayList<Producer>();
	  private List<Consumer> konsumenci = new ArrayList<Consumer>();
	  private Buffer bufor;
	  private VisualPanel VPanel = new VisualPanel();
	  
	  
	  private JMenuBar menubar = new JMenuBar();
	  private final JMenu menuPlik = new JMenu("Plik");
	  private final JMenu menuPomoc = new  JMenu("Pomoc");
	  private final JMenuItem manuZakoncz =  new JMenuItem("Zakoncz"); // literowka w nazwie zmiennej
	  private final JMenuItem manuInfo  =new JMenuItem("Informacje");
	  private final JMenuItem manuInstrukcja = new JMenuItem("O programie");
	  private final JMenuItem manuRestart = new JMenuItem("Restart");
	  private final JLabel label1 = new JLabel("Rozmiar bufora:");
	  private final JLabel label2 = new JLabel("Ilosc producentow:");
	  private final JLabel label3 = new JLabel("Ilosc konsumentow:");
	  private final JComboBox<Integer> buforComboBox= new JComboBox();
	  private final JComboBox<Integer> producenciComboBox = new JComboBox();
	  private final JComboBox<Integer> konsumenciComboBox = new JComboBox();
	  private final JButton start = new JButton("Start");
	  private final JButton stop = new JButton("Wstrzymaj symulacje");
	  
	  //Ramki 
	  private final Component liniaPozioma1 = Box.createHorizontalStrut(700);
	  private final Component liniaPozioma2 = Box.createHorizontalStrut(700);
	  private final Component liniaPozioma3 = Box.createHorizontalStrut(700);
	  private final Component liniaPionowa1 = Box.createHorizontalStrut(600);
	  private final Component liniaPionowa2 = Box.createVerticalStrut(280);
	  private JPanel panel = new JPanel();
	  private JTextArea history = new JTextArea();
	  
	  
	  public ProducentKonsument()
	  {
		  super("ProducentKonsumentSymulacja - Aplikacja z interfejsem graficznym");
		  setResizable(false);
		  setSize(650, 700);
		  setDefaultCloseOperation(3);
		  setLocationRelativeTo(null);
		  
		  manuZakoncz.addActionListener(this);
		  manuInfo.addActionListener(this);
		  manuInstrukcja.addActionListener(this);
		  manuRestart.addActionListener(this);
		  
		  menuPlik.add(manuRestart);
		  menuPlik.add(manuZakoncz);
		  menuPomoc.add(manuInfo);
		  menuPomoc.add(manuInstrukcja);
		  
		    for (int i = 1; i < 9; i++) 
		    {
		        this.buforComboBox.addItem(Integer.valueOf(i));
		    }
		    for (int i = 1; i < 9; i++)
		    {
		        this.producenciComboBox.addItem(Integer.valueOf(i));
		        this.konsumenciComboBox.addItem(Integer.valueOf(i));
		    }
		  
		    this.buforComboBox.addActionListener(this);
		    this.producenciComboBox.addActionListener(this);
		    this.konsumenciComboBox.addActionListener(this);
		    this.start.addActionListener(this);
		    this.stop.addActionListener(this);
		    this.panel.add(this.label1);
		    this.panel.add(this.buforComboBox);
		    this.panel.add(this.label2);
		    this.panel.add(this.producenciComboBox);
		    this.panel.add(this.label3);
		    this.panel.add(this.konsumenciComboBox);
		    this.panel.add(this.liniaPozioma1);
		    this.history.setEditable(false);
		    this.history.setFont(new Font("", 0, 12));
		    this.history.setColumns(32);
		    this.history.setRows(16);
		    JScrollPane scrollPane = new JScrollPane(this.history);
		    scrollPane.setHorizontalScrollBarPolicy(31);
		    this.panel.add(scrollPane);
		    this.panel.add(this.liniaPozioma2);
		    this.panel.add(this.start);
		    this.panel.add(this.stop);
		    this.panel.add(this.liniaPozioma3);
		    this.VPanel.add(this.liniaPionowa1);
		    this.VPanel.add(this.liniaPionowa2);
		    this.panel.add(this.VPanel);
		    
		    
		    setContentPane(this.panel);
		    
		    
		  menubar.add(menuPlik);
		  menubar.add(menuPomoc);
		  setJMenuBar(menubar);
		  setVisible(true);
	  }
	  
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		Object source = e.getSource();
		
	    if (source == this.start)
	    {

	        if (!this.started)
	        {
	          this.bufor = new Buffer(this.history, this.VPanel, ((Integer)this.buforComboBox.getSelectedItem()).intValue(), ((Integer)this.konsumenciComboBox.getSelectedItem()).intValue(), ((Integer)this.producenciComboBox.getSelectedItem()).intValue());
	          
	          for (int i = 1; i <= ((Integer)this.producenciComboBox.getSelectedItem()).intValue(); i++) {
	            this.producenci.add(new Producer(this.bufor, i));
	          }
	          
	          for (int i = 1; i <= ((Integer)this.konsumenciComboBox.getSelectedItem()).intValue(); i++) {
	            this.konsumenci.add(new Consumer(this.bufor, i));
	          }
	          
	          
	          for (Producer p : producenci) {
	            p.start();
	          }
	          
	          for (Consumer C : konsumenci) {
	            C.start();
	          }
	          
	          
	          this.started = true;
	          this.buforComboBox.setEnabled(false);
	          this.producenciComboBox.setEnabled(false);
	          this.konsumenciComboBox.setEnabled(false);
	        }
	        else
	        {
	          for (Producer p : producenci) {
	            p.play();
	          }

	          for (Consumer c : konsumenci) {
	            c.play();
	          }
	        }
	      }
		if (source == manuInfo)
		{
			JOptionPane.showMessageDialog(this, "Adam Cierniak\n"
					+ "Nr. 241310\n"
					+ "grudzien 2018");
			
		}
		if (source == this.stop)
	    {
	      for (Producer p : producenci)
	      {
	       p.pause();
	      }
	      for (Consumer c : konsumenci) {
	        c.pause();
	      }
	    }
		if(source == manuRestart)
		{
			dispose();
			new ProducentKonsument();
		}
		if(source == manuInstrukcja)
		{
			JOptionPane.showMessageDialog(this, "Program pokazuje dzialanie watkow.\nElement majacy aktualnie dostep do bufora jest zaznaczany  kolorem czeronym");
		}
		if (source == manuZakoncz)
		{
			dispose();
		}
		
	}

}
