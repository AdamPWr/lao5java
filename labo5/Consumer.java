package labo5;

public class Consumer extends Thread
{
	 private Buffer bufor;
	  private int id;
	  private boolean paused;
	  
	  public void play()
	  {
	    this.paused = false;
	  }
	  
	  public void pause()
	  {
	    this.paused = true;
	  }
	  
	  public Consumer(Buffer c, int i)
	  {
	    this.bufor = c;
	    this.id = i;
	  }
	  
	  public void run()
	  {
		while(this.paused == false)
		{
	    this.bufor.get(this.id);
	    try
	    {
	      sleep((int)(Math.random() * 1000.0D) + 200);
	    }
	    catch (InterruptedException e) {}
		}
	    while (this.paused) 
	    {
	      try
	      {
	        sleep(5L);
	      }
	      catch (InterruptedException e) {}
	    }
		
	  }
}
