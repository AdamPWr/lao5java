package labo5;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javax.swing.JTextArea;

public class Buffer 
{
	private BlockingQueue<Integer> content;
	  private int customers;
	  private int producers;
	  private int size;
	  private VisualPanel VPanel;
	  private JTextArea out;
	  
	  public Buffer(JTextArea TArea, VisualPanel VPanel, int size, int costumers, int producers)
	  {
	    this.out = TArea;
	    this.VPanel = VPanel;
	    this.size = size;
	    this.content = new ArrayBlockingQueue(size, true);
	    this.VPanel.changeBufferSize(size);
	    this.customers = costumers;
	    this.producers = producers;
	  }

	  public synchronized void get(int id)
	  {
	    this.out.append("Konsument #" + id + ": Chce cos skonsumowac.\n");
	    this.out.setCaretPosition(this.out.getDocument().getLength());
	    
	    
	    while (this.content.isEmpty())
	    {
	      this.out.append("Konsument #" + id + ": Bufor jest pusty wiec czekam.\n");
	      this.out.setCaretPosition(this.out.getDocument().getLength());
	      
	      this.VPanel.prepareSituation((Integer[])this.content.toArray(new Integer[0]), this.customers, this.producers, true, id, 1);
	      this.VPanel.repaint();
	      try
	      {
	        Thread.sleep(50L);
	      }
	      catch (InterruptedException e1) {}
	      try
	      {
	        wait();
	      }
	      catch (InterruptedException e) {}
	    }
	    this.VPanel.prepareSituation((Integer[])this.content.toArray(new Integer[0]), this.customers, this.producers, true, id, 2);
	    this.VPanel.repaint();
	    try
	    {
	      Thread.sleep(150L);
	      char t = (char)((Integer)this.content.take()).intValue();
	      this.out.append("Konsument #" + id + ": Zabieram " + t + ".\n");
	      this.out.setCaretPosition(this.out.getDocument().getLength());
	    }
	    catch (InterruptedException e) {}
	    this.VPanel.prepareSituation((Integer[])this.content.toArray(new Integer[0]), this.customers, this.producers, true, id, 0);
	    this.VPanel.repaint();
	    this.out.append("Konsument #" + id + ": Konsumuje.\n");
	    this.out.setCaretPosition(this.out.getDocument().getLength());
	    notifyAll();
	  }
	  
	  
	  
	  public synchronized void put(int id, char stuff)
	  {
	    this.out.append("Producent #" + id + ": Chce oddac " + stuff + ".\n");
	    this.out.setCaretPosition(this.out.getDocument().getLength());
	    while (this.content.size() == this.size)
	    {
	      this.out.append("Producent #" + id + ": Bufor jest zajety wiec czekam.\n");
	      this.out.setCaretPosition(this.out.getDocument().getLength());
	      this.VPanel.prepareSituation((Integer[])this.content.toArray(new Integer[0]), this.customers, this.producers, false, id, 1);
	      this.VPanel.repaint();
	      try
	      {
	        Thread.sleep(50L);
	        wait();
	      }
	      catch (InterruptedException e) {}
	    }
	    try
	    {
	      this.content.put(Integer.valueOf(stuff));
	    }
	    catch (InterruptedException e) {}
	    this.VPanel.prepareSituation((Integer[])this.content.toArray(new Integer[0]), this.customers, this.producers, false, id, 2);
	    this.VPanel.repaint();
	    try
	    {
	      Thread.sleep(150L);
	    }
	    catch (InterruptedException e) {}
	    this.out.append("Producent #" + id + ": Oddalem " + stuff + ".\n");
	    this.out.setCaretPosition(this.out.getDocument().getLength());
	    this.VPanel.prepareSituation((Integer[])this.content.toArray(new Integer[0]), this.customers, this.producers, false, id, 0);
	    this.VPanel.repaint();
	    this.out.append("Producent #" + id + ": Produkuje.\n");
	    this.out.setCaretPosition(this.out.getDocument().getLength());
	    notifyAll();
	  }
}
