package labo5;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;

import javax.swing.JPanel;

public class VisualPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	
	  private int rozmiarBufora;
	  private Integer[] bufferContent;
	  private int iloscKonsumentow;
	  private int iloscProducentow;
	  private boolean aktywnyUrzytkownik; // false oznacza producenta true konsumenta 
	  private int cpid;
	  private int[] cProgresses = new int[7];
	  private int[] pProgresses = new int[7];
	  
	  public void changeBufferSize(int size)
	  {
	    this.rozmiarBufora = size;
	  }
	  
	  public void prepareSituation(Integer[] content, int konsumenci, int producenci, boolean urzytkownik, int id, int h)
	  {
	    this.bufferContent = content;
	    this.iloscKonsumentow = konsumenci;
	    this.iloscProducentow = producenci;
	    this.aktywnyUrzytkownik = urzytkownik;
	    this.cpid = (id - 1);
	    if (this.aktywnyUrzytkownik) {
	      this.cProgresses[this.cpid] = h;
	    } else {
	      this.pProgresses[this.cpid] = h;
	    }
	  }
	  
	  public void drawSituation(Graphics g)
	  {
	    int box = 36;
	    Graphics2D g2 = (Graphics2D)g;
	    Stroke t = g2.getStroke();
	    g2.setColor(Color.GRAY);
	    g2.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
	    
	    g2.setStroke(new BasicStroke(2));
	    g2.setColor(Color.MAGENTA);
	    
	    if (this.rozmiarBufora > 0)
	    {
	      if (this.bufferContent.length == this.rozmiarBufora) {
	        g2.fillRect(getWidth() / 2 - this.rozmiarBufora * box / 2, getHeight() / 2 - box / 2, this.rozmiarBufora * box, box);
	      } else {
	        g2.drawRect(getWidth() / 2 - this.rozmiarBufora * box / 2, getHeight() / 2 - box / 2, this.rozmiarBufora * box, box);
	      }
	      g2.setColor(Color.BLACK);
	      String s = "";
	      for (int i = this.rozmiarBufora - 1; i >= 0; i--) {
	        if (i < this.bufferContent.length) {
	          s = s + "|" + (char)this.bufferContent[i].intValue() + "|";
	        } else {
	          s = s + "| |";
	        }
	      }
	      g2.setFont(new Font("Monospaced", 1, 20));
	      g2.drawString(s, getWidth() / 2 - this.rozmiarBufora * box / 2, getHeight() / 2 + 5);
	    }
	    for (int i = 0; i < this.iloscProducentow; i++)
	    {
	      int w;
	      if (this.pProgresses[i] == 2)
	      {
	        w = 130;
	      }
	      else
	      {
	        if (this.pProgresses[i] == 1) {
	          w = 80;
	        } else {
	          w = 0;
	        }
	      }
	      g2.setColor(Color.GREEN);
	      if ((!this.aktywnyUrzytkownik) && (this.cpid == i)) {
	        g2.setColor(Color.RED);
	      }
	      g2.fillOval(w, getHeight() / 2 - this.iloscProducentow * box / 2 + i * box, box, box);
	    }
	    for (int i = 0; i < this.iloscKonsumentow; i++)
	    {
	      int w;
	      if (this.cProgresses[i] == 2)
	      {
	        w = getWidth() - 130 - box;
	      }
	      else
	      {
	        if (this.cProgresses[i] == 1) {
	          w = getWidth() - 80 - box;
	        } else {
	          w = getWidth() - box;
	        }
	      }
	      g2.setColor(Color.BLUE);
	      if ((this.aktywnyUrzytkownik) && (this.cpid == i)) {
	        g2.setColor(Color.RED);
	      }
	      g2.fillOval(w, getHeight() / 2 - this.iloscKonsumentow * box / 2 + i * box, box, box);
	    }
	    g2.setStroke(t);
	  }
	  
	  public void paintComponent(Graphics g)
	  {
	    super.paintComponent(g);
	    drawSituation(g);
	  }
}
